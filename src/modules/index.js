import express from 'express'
import Admin from './admin/route'
import User from './user/route'
import * as API from '../constants/api'

const route = express()

route.use(API.ADMIN_URL, Admin)
route.use(API.USER_URL, User)

export default route
