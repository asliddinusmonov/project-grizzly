import express from 'express'
import { middleware as body } from 'bodymen'
import { schema } from '../model'
import userCreate from '../controllers/userCreate'

const { email, password, name } = schema.tree

const router = express.Router()

router
    .route('/create')
    .post(body({email, password, name}), userCreate)

export default router
