import mongoose, { Schema } from 'mongoose'

const roles = ['user', 'admin', 'superadmin']

const userSchema = new Schema({
    email: {
        type: String,
        match: /^\S+@\S+\.\S+$/,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        minlength: 4
    },
    role: {
        type: String,
        enum: roles,
        default: 'user'
    },
    active: {
        type: Boolean,
        default: true
    },
    name: {
        type: String,
        required: true,
        index: true,
        trim: true
    },
    picture: {
        type: String,
        trim: true
    }
}, {
    timestamps: true
})

const model = mongoose.model('User', userSchema)

export const schema = model.schema
export default model
