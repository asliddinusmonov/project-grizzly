import express from 'express'
import { middleware as body } from 'bodymen'
import { schema } from '../model'
import adminCreate from '../controllers/adminCreate'
import adminAuth from '../controllers/adminAuth'
import adminUnauth from '../controllers/adminUnauth'
import passportAuth from '../../../services/passport/adminLocal'
import passportSecure from '../../../services/passport/adminJwt'

const { email, password, name } = schema.tree

const router = express.Router()

router
    .route('/register')
    .post(passportSecure, body({email, password, name}), adminCreate)

router
    .route('/auth')
    .post(passportAuth, adminAuth)

router
    .route('/unauth')
    .delete(passportSecure, adminUnauth)

export default router
