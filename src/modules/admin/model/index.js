import _ from 'lodash'
import mongoose, { Schema } from 'mongoose'
import bcrypt from 'bcrypt-nodejs'

const roles = ['admin', 'superadmin']

const adminSchema = new Schema({
    email: {
        type: String,
        match: /^\S+@\S+\.\S+$/,
        required: true,
        unique: true,
        trim: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: roles,
        default: 'admin'
    },
    active: {
        type: Boolean,
        default: true
    },
    name: {
        type: String,
        required: true,
        index: true,
        trim: true
    }
}, {
    timestamps: true
})

adminSchema.pre('save', function (next) {
    const user = this

    bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err)

        return bcrypt.hash(user.password, salt, null, (error, hash) => {
            if (error) return next(error)

            user.password = hash
            return next()
        })
    })
})

adminSchema.methods.comparePassword = function (candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) return callback(err)

        return callback(null, isMatch)
    })
}

const model = mongoose.model('Admin', adminSchema)

export const schema = model.schema
export default model