import {signOutUser} from '../../../services/jwt'

const adminUnauth = (req, res) => {
    signOutUser(req.headers.authorization)
    return res.send({message: 'deleted'})
}

export default adminUnauth
