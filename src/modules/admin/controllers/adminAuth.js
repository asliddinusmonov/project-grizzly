import Admin from '../model'
import { signInUser } from '../../../services/jwt'

const adminAuth = (req, res) =>
    Admin.findOne({email: req.body.email})
        .then(user => {
            res.send({token: signInUser(user)})
        })
        .catch(error => {
            res.send({error: error})
        })

export default adminAuth



