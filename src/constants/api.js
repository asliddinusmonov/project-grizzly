export const API = 'api'
export const API_V1 = 'v1'
export const API_URL = `/${API}/${API_V1}`

// docs
export const DOCS = 'docs'
export const DOCS_URL = `/${DOCS}`

// Admin
export const ADMIN = 'admin'
export const ADMIN_URL = `/${ADMIN}`
export const ADMIN_AUTH = 'auth'
export const ADMIN_AUTH_URL = `/${ADMIN_AUTH}`
export const ADMIN_UNAUTH = 'unauth'
export const ADMIN_UNAUTH_URL = `/${ADMIN_UNAUTH}`
export const ADMIN_REGISTER = 'register'
export const ADMIN_REGISTER_URL = `/${ADMIN_REGISTER}`

// User
export const USER = 'user'
export const USER_URL = `/${USER}`
