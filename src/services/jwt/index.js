import jwt from 'jwt-simple'
import _ from 'lodash'
import config from '../../../config'

export const signInUser = (user) => {
    const timestamps = new Date().getTime()
    return jwt.encode(
        {
            sub: _.get(user, 'id'),
            iat: timestamps
        },
        _.get(config, 'secret')
    )
}

export const signOutUser = (token) => {
    const decode = jwt.decode(token, _.get(config, 'secret'))
    return decode
}