import Promise from 'bluebird'
import mongoose from 'mongoose'
import beautifyUnique from 'mongoose-beautiful-unique-validation'

const mongooseConnect = (config) => {
    mongoose.Promise = Promise
    mongoose.set('debug', config.mongooseDebug)
    mongoose.plugin(beautifyUnique)

    mongoose.connect(config.uri, { useMongoClient: true })

    mongoose.connection.on('error', (err) => {
        console.error('MongoDB connection error: ' + err)
        process.exit(-1)
    })

    return mongoose
}

export default mongooseConnect
