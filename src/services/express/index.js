import express from 'express'
import cors from 'cors'
import compression from 'compression'
import logger from 'morgan'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import { errorHandler as queryErrorHandler } from 'querymen'
import { errorHandler as bodyErrorHandler } from 'bodymen'
import swaggerUi from 'swagger-ui-express'
import config from '../../../config'

export default (apiRoot, api, swaggerDocument) => {
    const app = express()

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access.Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS')
        res.header('Access-Control-Allow-Headers', 'Origin, X Requested With, Content-Type, Accept')
        app.disable('x-powered-by')
        next()
    })
    app.use(cors(config.corsOptions))
    app.use(compression())
    app.use(logger('dev'))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(cookieParser())

    app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

    app.use(apiRoot, api)
    app.use(queryErrorHandler())
    app.use(bodyErrorHandler())

    return app
}
