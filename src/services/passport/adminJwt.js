import _ from 'lodash'
import passport from 'passport'
import { Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt'
import Admin from '../../modules/admin/model'
import config from '../../../config'

const ADMIN_JWT = 'admin_jwt'

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: _.get(config, 'secret')
}

passport.use(ADMIN_JWT, new JwtStrategy(jwtOptions,
    (payload, done) => {
        Admin.findById(_.get(payload, 'sub'), (err, user) => {
            if (err) return done(err, false)

            if (user) return done(null, user)

            return done(null, false, {message: 'user is not authenticated'})
        })
    }
))

const adminJwtSecure = (req, res, next) =>
    passport.authenticate(ADMIN_JWT, { session: false }, (err, user, info) => {
        if (err) return res.json({error: err})
        if (!user) return res.json({message: 'user is not authenticated'})
        next()
    })(req, res, next)

export default adminJwtSecure
