import _ from 'lodash'
import passport from 'passport'
import LocalStrategy from 'passport-local'
import { Schema } from 'bodymen'
import Admin, { schema } from '../../modules/admin/model'

const ADMIN_LOCAL = 'admin_local'

const localOptions = {
    usernameField: 'email',
    session: false
}

passport.use(ADMIN_LOCAL, new LocalStrategy(localOptions,
    (email, password, done) => {
        const userSchema = new Schema({
            email: schema.tree.email,
            password: schema.tree.password
        })

        userSchema.validate({ email, password }, (err) => {
            if (err) done(err)
        })

        Admin.findOne({ email })
            .then((user) => {
                if (!user) return done(null, false, {message: 'user is not found'})

                return user.comparePassword(password, (error, isMatch) => {
                    if (error) return done(error)
                    if (!isMatch) return done(null, false)

                    return done(null, user)
                })
            })
            .catch(done)
    })
)

const adminLocalSecure = (req, res, next) =>
    passport.authenticate(ADMIN_LOCAL, { session: false }, (err, user, info) => {
        if (err) return res.json({error: err})
        if (!user) return res.json({message: _.get(info, 'message')})
        next()
    })(req, res, next)


export default adminLocalSecure
