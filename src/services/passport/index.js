import _ from 'lodash'
import passport from 'passport'
import LocalStrategy from 'passport-local'
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt'
import { Schema } from 'bodymen'
import User, { schema } from '../../modules/user/model'
import config from '../../../config'

export const local = (req, res, next) => {
    passport.authenticate('admin_local', { session: false }, (err, user, info) => {
        if (err) return res.json(err)
        if (!user) return res.json({message: info.message})
        next()
    })(req, res, next)
}

export const jwt = passport.authenticate('jwt', { session: false })

const localOptions = {
    usernameField: 'email'
}

passport.use('admin_local', new LocalStrategy(localOptions, (email, password, done) => {
    done(null, {message: 'user is not!'})
}))

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: _.get(config, 'secret')
}

passport.use(new JwtStrategy(jwtOptions, (payload, done) => {

}))
