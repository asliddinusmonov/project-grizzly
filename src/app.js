import YAML from 'yamljs'
import mongoose from './services/mongoose'
import express from './services/express'
import api from './modules'
import config from '../config'
import { API_URL } from './constants/api'

const swaggerDocument = YAML.load('./swagger/swagger.yaml')

const app = express(API_URL, api, swaggerDocument)

mongoose(config.mongo)

module.exports = app
export default module.exports
