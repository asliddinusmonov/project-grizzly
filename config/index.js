const config = {
    NODE_ENV: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8000,
    // secret
    secret: 'secret',
    // mongoose
    mongo: {
        uri: 'mongodb://localhost/newBase',
        mongooseDebug: true,
    },
    // email
    email: {
        user: 'asl180493@gmail.com',
        password: 'avatar6864'
    },
    // cors
    corsOptions: {
        origin: 'http://localhost:3000',
        optionsSuccessStatus: 200
    }
}

module.exports = config